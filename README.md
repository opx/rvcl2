Beta component (trial & error) of Inherited Components from DevExpress

* Looks like standard LabeledEdit, support Label & secondary text box for ButtonedEdit
* Next tab use ENTER, go previous tab use ESC
* Autosize width of input based on max length (use spesific font such as Courier, Consolas etc)

Additional Properties

LabelProps
property Font: TFont 
property Properties: TcxLabelProperties 
property Visible: Boolean 
property Caption: string
property Position: TRLabelPosition 
property PosAlign: TRLabelAlign 
property Space: Integer 

ExtProps
property AutoSizeField: Boolean 
property NextEnabled: Boolean 
property PrevEnabled: Boolean 
property PasteEnabled: Boolean 
property PopupEnabled: Boolean 

Event
OnTriggerValidate(Sender: TObject; var AllowExit: Boolean)