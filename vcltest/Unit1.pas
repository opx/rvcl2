unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxSkinsCore, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2010Blue, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinTheBezier, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils, cxMaskEdit, RVclEdit, cxButtonEdit,
  cxCurrencyEdit, cxDropDownEdit, cxCalendar, cxTextEdit, cxCheckBox, RVclExt,
  Vcl.StdCtrls, cxButtons, cxClasses, dxSkinsForm;

type
  TForm1 = class(TForm)
    dxSkinController1: TdxSkinController;
    RComboBox1: TRComboBox;
    RDateEdit1: TRDateEdit;
    REditBtn1: TREditBtn;
    REdit1: TREdit;
    RNumEdit1: TRNumEdit;
    REdit2: TREdit;
    REdit3: TREdit;
    REdit4: TREdit;
    REdit5: TREdit;
    REdit6: TREdit;
    REdit7: TREdit;
    REdit8: TREdit;
    REdit9: TREdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

end.
