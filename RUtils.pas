unit RUtils;

interface

uses
  Vcl.Graphics, SysUtils, Classes, Variants;

function GetTextWidth(xFont: TFont; xTxt: string): Integer;
function VariantToStr(const xData: string): String;
function VariantToInt(const xData: Variant): Integer;

implementation

function GetTextWidth(xFont: TFont; xTxt: string): Integer;
var
  c: TBitmap;
begin
  c := TBitmap.Create;
  try
    c.Canvas.Font.Assign(xFont);
    Result:= c.Canvas.TextWidth(xTxt);
  finally
    c.Free;
  end;
end;

function VariantToStr(const xData: string): String;
begin
  if not VarIsNull(xData) then
  begin
    Result:= String(xData);
  end
  else
  begin
    Result:= '';
  end;
end;

function VariantToInt(const xData: Variant): Integer;
begin
  if not VarIsNull(xData) then
  begin
    Result:= Integer(xData);
  end
  else
  begin
    Result:= 0;
  end;
end;

end.
