unit RClasses;

interface

uses
  SysUtils, Classes, cxLabel, Messages, Vcl.Controls, Variants,
  Windows, Vcl.Graphics, cxLookAndFeels, Vcl.Forms,
  cxButtonEdit, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxButtons, cxCurrencyEdit, RUtils, RConfig;

type
  TOnTriggerKeyTab = procedure(Sender: TObject; var TabMove: Boolean) of Object;
  TOnTriggerValidate = procedure(Sender: TObject; AllowExit: Boolean = True) of Object;
  TOnTriggerKey = procedure(Sender: TObject) of Object;
  TRLabelAlign = (laFirst, laMid, laLast);
  TRLabelPosition = (lpLeft = 0, lpTop, lpRight, lpBottom);
  TRText2Position = (ltRight, ltBottom);

 TRCbxItem = class
  protected
    FKey: Variant;
    FDisplayText: string;
  public
    property Key: Variant read FKey write FKey;
    property DisplayText: string read FDisplayText write FDisplayText;
    constructor Create(xKey: Variant; xDisplayText: string);
    destructor Destroy; override;
  end;

  TRLabelProps = class(TPersistent)
  private
    FObj: TcxLabel;
    FLabelPos: TRLabelPosition;
    FLabelAlign: TRLabelAlign;
    FOwner: TWinControl;
    FLabelSpace: Integer;
    function GetFont: TFont;
    procedure SetFont(xVal: TFont);
    function GetProperties: TcxLabelProperties;
    procedure SetProperties(xVal: TcxLabelProperties);
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    function GetCaption: string;
    procedure SetCaption(const Value: string);
    procedure SetLabelPosition(const Value: TRLabelPosition);
    procedure SetLabelPosAlign(const Value: TRLabelAlign);
    procedure SetLabelSpace(const Value: Integer);
  public
    constructor Create(AOwner, ATarget: TWinControl);
    procedure UpdateView;
  published
    property Font: TFont read GetFont write SetFont;
    property Properties: TcxLabelProperties read GetProperties write SetProperties;
    property Visible: Boolean read GetVisible write SetVisible;
    property Caption: string read GetCaption write SetCaption;
    property Position: TRLabelPosition read FLabelPos write SetLabelPosition;
    property PosAlign: TRLabelAlign read FLabelAlign write SetLabelPosAlign;
    property Space: Integer read FLabelSpace write SetLabelSpace;
  end;

  TRExtEditProps = class(TPersistent)
  private
    FObj: TWinControl;
    FAutoSizeField: Boolean;
    FNextEnabled: Boolean;
    FPrevEnabled: Boolean;
    FPasteEnabled: Boolean;
    FPopupEnabled: Boolean;
    FDisabledTextColor: TColor;
    FDisabledBgColor: TColor;
    procedure SetAutoSizeField(xVal: Boolean);
    function GetPopupEnabled: Boolean;
    procedure SetPopupEnabled(const Value: Boolean);
  public
    constructor Create(ATarget: TWinControl);
  published
    property AutoSizeField: Boolean read FAutoSizeField write SetAutoSizeField;
    property NextEnabled: Boolean read FNextEnabled write FNextEnabled;
    property PrevEnabled: Boolean read FPrevEnabled write FPrevEnabled;
    property PasteEnabled: Boolean read FPasteEnabled write FPasteEnabled;
    property PopupEnabled: Boolean read GetPopupEnabled write SetPopupEnabled;
  end;

  TRInfoTextProps = class(TPersistent)
  private
    FParent: TWinControl;
    FTarget: TcxCustomMaskEdit;
    FText2Pos: TRText2Position;
    FSpace: Integer;
    function GetPos: TRText2Position;
    function GetSpace: Integer;
    function GetText: string;
    function GetVisible: Boolean;
    procedure SetPos(const Value: TRText2Position);
    procedure SetSpace(const Value: Integer);
    procedure SetText(const Value: string);
    procedure SetVisible(const Value: Boolean);
    function GetMaxLen: Integer;
    procedure SetMaxLen(const Value: Integer);
    function GetWidth: Integer;
    procedure SetWidth(const Value: Integer);
  public
    constructor Create(AParent, ATarget: TWinControl);
    procedure UpdateView;
  published
    property Text: string read GetText write SetText;
    property Space: Integer read GetSpace write SetSpace;
    property Position: TRText2Position read GetPos write SetPos;
    property Visible: Boolean read GetVisible write SetVisible;
    property MaxLength: Integer read GetMaxLen write SetMaxLen;
    property Width: Integer read GetWidth write SetWidth;
  end;

  TRExtStdProps = class(TPersistent)
  private
    FObj: TWinControl;
    FNextEnabled: Boolean;
    FPrevEnabled: Boolean;
    function GetNextEnabled: Boolean;
    function GetPrevEnabled: Boolean;
    procedure SetNextEnabled(const Value: Boolean);
    procedure SetPrevEnabled(const Value: Boolean);
  public
    constructor Create(ATarget: TWinControl);
  published
    property NextEnabled: Boolean read GetNextEnabled write SetNextEnabled;
    property PrevEnabled: Boolean read GetPrevEnabled write SetPrevEnabled;
  end;

implementation

{ TRLabelProps }

constructor TRLabelProps.Create(AOwner, ATarget: TWinControl);
begin
  FObj:= TcxLabel(AOwner);
  FOwner:= ATarget;
  FObj.DoubleBuffered:= True;
end;

function TRLabelProps.GetCaption: string;
begin
  Result:= FObj.Caption;
end;

function TRLabelProps.GetFont: TFont;
begin
  Result:= FObj.Style.Font;
end;

function TRLabelProps.GetProperties: TcxLabelProperties;
begin
  Result:= FObj.Properties;
end;

function TRLabelProps.GetVisible: Boolean;
begin
  Result:= FObj.Visible;
end;

procedure TRLabelProps.SetCaption(const Value: string);
begin
  FObj.Caption:= Value;
  if FObj.Visible then FObj.Visible:= FObj.Caption <> '';
  FObj.Invalidate;
  UpdateView;
end;

procedure TRLabelProps.SetFont(xVal: TFont);
begin
  FObj.Style.Font:= xVal;
end;

procedure TRLabelProps.SetLabelPosAlign(const Value: TRLabelAlign);
begin
  if FLabelAlign <> Value then
  begin
    FLabelAlign:= Value;
    UpdateView;
  end;
end;

procedure TRLabelProps.SetLabelPosition(const Value: TRLabelPosition);
begin
  if FLabelPos <> Value then
  begin
    FLabelPos:= Value;
    UpdateView;
  end;
end;

procedure TRLabelProps.SetLabelSpace(const Value: Integer);
begin
  if FLabelSpace <> Value then
  begin
    FLabelSpace:= Value;
    UpdateView;
  end;
end;

procedure TRLabelProps.SetProperties(xVal: TcxLabelProperties);
begin
  if FObj.Properties <> xVal then
  begin
    FObj.Properties:= xVal;
    FObj.Invalidate;
    UpdateView;
  end;
end;

procedure TRLabelProps.SetVisible(const Value: Boolean);
begin
  FObj.Visible:= Value;
end;

procedure TRLabelProps.UpdateView;
begin

  case FLabelPos of
    lpLeft:
    begin
      Fobj.Left:= (FOwner.Left - Fobj.Width) - FLabelSpace;
      case FLabelAlign of
        laFirst:
        begin
          Fobj.Top:= FOwner.Top;
        end;
        laMid:
        begin
          Fobj.Top:= FOwner.Top + Round((FOwner.Height / 2) - (Fobj.Height / 2));
        end;
        laLast:
        begin
          Fobj.Top:= (FOwner.Height + FOwner.Top) - Fobj.Height;
        end;
      end;
    end;
    lpTop:
    begin
      Fobj.Top:= FOwner.Top - Fobj.Height - FLabelSpace;
      case FLabelAlign of
        laFirst:
        begin
          Fobj.Left:= FOwner.Left;
        end;
        laMid:
        begin
          Fobj.Left:= FOwner.Left + Round((FOwner.Width / 2) - (Fobj.Width / 2));
        end;
        laLast:
        begin
          Fobj.Left:= (FOwner.Width + FOwner.Left) - Fobj.Width;
        end;
      end;
    end;
    lpRight:
    begin
      Fobj.Left:= FOwner.Left + FOwner.Width + FLabelSpace;
      case FLabelAlign of
        laFirst:
        begin
          Fobj.Top:= FOwner.Top;
        end;
        laMid:
        begin
          Fobj.Top:= FOwner.Top + Round((FOwner.Height / 2) - (Fobj.Height / 2));
        end;
        laLast:
        begin
          Fobj.Top:= (FOwner.Height + FOwner.Top) - Fobj.Height;
        end;
      end;
    end;
    lpBottom:
    begin
      Fobj.Top:= FOwner.Top + FOwner.Height + FLabelSpace;
      case FLabelAlign of
        laFirst:
        begin
          Fobj.Left:= Self.FOwner.Left;
        end;
        laMid:
        begin
          Fobj.Left:= FOwner.Left + Round((FOwner.Width / 2) - (Fobj.Width / 2));
        end;
        laLast:
        begin
          Fobj.Left:= (FOwner.Width + FOwner.Left) - Fobj.Width;
        end;
      end;
    end;
  end;
end;

{ TRExtEditProps }

constructor TRExtEditProps.Create(ATarget: TWinControl);
begin
  FObj:= ATarget;
end;

function TRExtEditProps.GetPopupEnabled: Boolean;
begin
  Result:= FPopupEnabled;
end;

procedure TRExtEditProps.SetAutoSizeField(xVal: Boolean);
var
  xStr: string;
  xSz: Integer;
  xBtnWidth: Integer;
  i: Integer;
begin
  FAutoSizeField:= xVal;

  if not FAutoSizeField then Exit;

  if FObj.ClassName = 'TREdit' then
  begin
    if TcxMaskEdit(FObj).Properties.MaxLength <> 0 then
    begin
      xSz:= GetTextWidth(TcxMaskEdit(FObj).Style.Font, 'X');
      FObj.Width:= (xSz) * TcxMaskEdit(FObj).Properties.MaxLength + xSz + 5;
    end;
  end;

  if FObj.ClassName = 'TREditBtn' then
  begin
    if TcxButtonEdit(FObj).Properties.MaxLength <> 0 then
    begin
      xSz:= GetTextWidth(TcxButtonEdit(FObj).Style.Font, 'X');
      xBtnWidth:= 0;
      for i:= 0 to Pred(TcxButtonEdit(FObj).Properties.Buttons.Count) do
      begin
        xBtnWidth:= xBtnWidth + TcxButtonEdit(FObj).Properties.Buttons.Items[i].Width;
      end;
      FObj.Width:= (xSz) * TcxButtonEdit(FObj).Properties.MaxLength + xSz + 5 + xBtnWidth;
    end;
  end;

  if FObj.ClassName = 'TRNumEdit' then
  begin
    if TcxButtonEdit(FObj).Properties.MaxLength <> 0 then
    begin
      xSz:= GetTextWidth(TcxButtonEdit(FObj).Style.Font, 'X');
      xBtnWidth:= 0;
      for i:= 0 to Pred(TcxButtonEdit(FObj).Properties.Buttons.Count) do
      begin
        xBtnWidth:= xBtnWidth + TcxButtonEdit(FObj).Properties.Buttons.Items[i].Width;
      end;
      FObj.Width:= (xSz) * TcxButtonEdit(FObj).Properties.MaxLength + xSz + 5 + xBtnWidth;
    end;
  end;
end;

procedure TRExtEditProps.SetPopupEnabled(const Value: Boolean);
begin
  FPopupEnabled:= Value;
end;

{ TRInfoTextProps }

constructor TRInfoTextProps.Create(AParent, ATarget: TWinControl);
begin
  FParent:= TcxCustomButtonEdit(AParent);
  FTarget:= TcxCustomMaskEdit(ATarget);

  FTarget.Properties.ReadOnly:= True;
  FTarget.TabStop:= False;
end;

function TRInfoTextProps.GetMaxLen: Integer;
begin
  Result:= FTarget.Properties.MaxLength;
end;

function TRInfoTextProps.GetPos: TRText2Position;
begin
  Result:= FText2Pos;
end;

function TRInfoTextProps.GetSpace: Integer;
begin
  Result:= FSpace;
end;

function TRInfoTextProps.GetText: string;
begin
  Result:= FTarget.Text;
end;

function TRInfoTextProps.GetVisible: Boolean;
begin
  Result:= FTarget.Visible;
end;

procedure TRInfoTextProps.SetMaxLen(const Value: Integer);
begin
  FTarget.Properties.MaxLength:= Value;
end;

procedure TRInfoTextProps.SetPos(const Value: TRText2Position);
begin
  if GetPos <> Value then
  begin
    FText2Pos:= Value;
    UpdateView;
  end;
end;

procedure TRInfoTextProps.SetSpace(const Value: Integer);
begin
  if FSpace <> Value then
  begin
    FSpace:= Value;
    UpdateView;
  end;
end;

procedure TRInfoTextProps.SetText(const Value: string);
begin
  FTarget.Text:= Value;
end;

procedure TRInfoTextProps.SetVisible(const Value: Boolean);
begin
  FTarget.Visible:= Value;
end;

procedure TRInfoTextProps.UpdateView;
begin
  case GetPos of
    ltRight:
    begin
      FTarget.Left:= FParent.Left + FParent.Width + FSpace;
      FTarget.Top:= FParent.Top;
    end;
    ltBottom:
    begin
      FTarget.Left:= FParent.Left;
      FTarget.Top:= FParent.Top + FParent.Height + FSpace;
    end;
  end;
end;

function TRInfoTextProps.GetWidth;
begin
  Result:= FTarget.Width;
end;

procedure TRInfoTextProps.SetWidth(const Value: Integer);
begin
  FTarget.Width:= Value;
end;

{ TRExtStdProps }

constructor TRExtStdProps.Create(ATarget: TWinControl);
begin
  FObj:= ATarget;
end;

function TRExtStdProps.GetNextEnabled: Boolean;
begin
  Result:= FNextEnabled;
end;

function TRExtStdProps.GetPrevEnabled: Boolean;
begin
  Result:= FPrevEnabled;
end;

procedure TRExtStdProps.SetNextEnabled(const Value: Boolean);
begin
  if FObj.ClassName = 'TRButton' then
  begin
    FNextEnabled:= False;
  end
  else
  begin
    FNextEnabled:= Value;
  end;
end;

procedure TRExtStdProps.SetPrevEnabled(const Value: Boolean);
begin
  FPrevEnabled:= Value;
end;

{ TRCbxItem }

constructor TRCbxItem.Create(xKey: Variant; xDisplayText: string);
begin
  FKey:= xKey;
  FDisplayText:= xDisplayText;
end;

destructor TRCbxItem.Destroy;
begin
  inherited;
end;

end.
