unit RCompReg;

interface

uses
  Classes,
  RVclEdit, RVclExt, RVclGrid, RVclLookup;

procedure Register;

implementation

procedure register;
begin
  RegisterComponents('MyComp',
    [TREdit, TREditBtn, TRNumEdit, TRDateEdit, TRComboBox, TRCheckBox, TRButton,
     TRVGrid, TRGrid, TRSpinEdit, TRMemo, TRLookupComboBox, TRListBox]);
end;

end.
