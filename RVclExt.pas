unit RVclExt;

interface

uses
  SysUtils, Classes, cxCheckBox, cxButtons, cxVGrid,
  Windows, Vcl.StdCtrls, Vcl.Controls, Vcl.Forms, Messages,
  RClasses, RTypes, cxCurrencyEdit, cxClasses, cxEdit, cxTextEdit, cxCalendar,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxLabel, cxListBox, ExtCtrls;

type
  TRCheckBox = class(TcxCheckBox)
  private
    FOnTriggerValidate: TOnTriggerValidate;
    FExtProps: TRExtStdProps;
    FCID: Integer;
    procedure SetCheckBoxProps(const Value: TRExtStdProps);
    procedure InitObjects;
  protected
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property CID: Integer read FCID write FCID;
    property ExtProps: TRExtStdProps read FExtProps write SetCheckBoxProps;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidate write FOnTriggerValidate;
  end;

  TRButton = class(TcxButton)
  private
    FCID: Integer;
    FOnTriggerValidate: TOnTriggerValidate;
    FExtProps: TRExtStdProps;
    procedure SetButtonProps(const Value: TRExtStdProps);
    procedure InitObjects;
  protected
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property CID: Integer read FCID write FCID;
    property ExtProps: TRExtStdProps read FExtProps write SetButtonProps;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidate write FOnTriggerValidate;
  end;

  TRVGrid = class(TcxVerticalGrid)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetValue(AIndex: Integer; AValue: Variant);
    procedure AddEditor(ACaption: string; AType: TREditorTyp; AValue: Variant; AOpts: TREditorOpts = []);
    procedure ResetRows;
    procedure ResetRows2;
  end;

  TListBoxSelectionChangedEvent = procedure(Sender: TObject; APreviousIndex, ACurrentIndex: Integer) of object;
  TRListBox = class(TcxListBox)
  private
    FTempIndex: Integer;
    FTimer: TTimer;
    FSelectionChanged: TListBoxSelectionChangedEvent;
    procedure TimerOnTimer(Sender: TObject);
  published
    property OnSelectionChanged: TListBoxSelectionChangedEvent read FSelectionChanged write FSelectionChanged;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ClearExt;
  end;

implementation

{ TRCheckBox }

procedure TRCheckBox.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;
  if Assigned(FOnTriggerValidate) then
  begin
    FOnTriggerValidate(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

constructor TRCheckBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Cursor:= crHandPoint;
  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;
  Transparent:= True;

  InitObjects;
end;

destructor TRCheckBox.Destroy;
begin
  FExtProps.Free;
  inherited;
end;

procedure TRCheckBox.InitObjects;
begin
  FExtProps:= TRExtStdProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
end;

procedure TRCheckBox.KeyDown(var Key: Word; Shift: TShiftState);
var
  isMoveTab: Boolean;
begin
  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_ESCAPE) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;

  inherited KeyDown(Key, Shift);
end;

procedure TRCheckBox.SetCheckBoxProps(const Value: TRExtStdProps);
begin
  FExtProps := Value;
end;

{ TRButton }

procedure TRButton.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;
  if Assigned(FOnTriggerValidate) then
  begin
    FOnTriggerValidate(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

constructor TRButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Cursor:= crHandPoint;

  Font.Name:= 'Verdana';
  Font.Size:= 8;

  InitObjects;
end;

destructor TRButton.Destroy;
begin
  FExtProps.Free;
  inherited;
end;

procedure TRButton.InitObjects;
begin
  FExtProps:= TRExtStdProps.Create(Self);
  FExtProps.NextEnabled:= False;
  FExtProps.PrevEnabled:= True;
end;

procedure TRButton.KeyDown(var Key: Word; Shift: TShiftState);
var
  isMoveTab: Boolean;
begin
  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_ESCAPE) then
    begin
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
      Key:= 0;
    end;
  end;

  inherited KeyDown(Key, Shift);
end;

procedure TRButton.SetButtonProps(const Value: TRExtStdProps);
begin
  FExtProps := Value;
end;

{ TRVertGrid }

procedure TRVGrid.AddEditor(ACaption: string; AType: TREditorTyp;
  AValue: Variant; AOpts: TREditorOpts = []);
var
  o: TcxEditorRow;
begin
  o:= Add(TcxEditorRow) as TcxEditorRow;
  with o.Properties do
  begin
    Caption:= ACaption;
    Value:= AValue;

    if eoNoFocus in AOpts then o.Options.Focusing:= False;

    case AType of
      etDate:
      begin
        EditPropertiesClass:= TcxCustomEditPropertiesClass(TcxDateEditProperties);
        with TcxDateEditProperties(EditProperties) do
        begin
          DisplayFormat:= 'dd/MM/yyyy';
          EditFormat:= 'dd/MM/yyyy';
        end;
      end;
      etNumber:
      begin
        EditPropertiesClass:= TcxCustomEditPropertiesClass(TcxCurrencyEditProperties);
        with TcxCurrencyEditProperties(EditProperties) do
        begin
          EditFormat:= '#0';
          DisplayFormat:= '#0';
          UseDisplayFormatWhenEditing:= True;
          UseThousandSeparator:= False;
          DecimalPlaces:= 0;
        end;
      end;
      etNumberFmt0:
      begin
        EditPropertiesClass:= TcxCustomEditPropertiesClass(TcxCurrencyEditProperties);
        with TcxCurrencyEditProperties(EditProperties) do
        begin
          EditFormat:= '#,###0';
          DisplayFormat:= '#,###0';
          UseDisplayFormatWhenEditing:= True;
          UseThousandSeparator:= True;
          DecimalPlaces:= 0;
        end;
      end;
      etNumberFmt2:
      begin
        EditPropertiesClass:= TcxCustomEditPropertiesClass(TcxCurrencyEditProperties);
        with TcxCurrencyEditProperties(EditProperties) do
        begin
          EditFormat:= '#,###0.00';
          DisplayFormat:= '#,###0.00';
          UseDisplayFormatWhenEditing:= True;
          UseThousandSeparator:= True;
          DecimalPlaces:= 2;
        end;
      end;
      else
      begin
        EditPropertiesClass:= TcxCustomEditPropertiesClass(TcxCustomTextEditProperties);
        with TcxCustomTextEditProperties(EditProperties) do
        begin
          Text:= '';
        end;
      end;
    end;


  end;
end;

constructor TRVGrid.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TRVGrid.Destroy;
begin

  inherited;
end;

procedure TRVGrid.ResetRows;
var
  i: Integer;
begin
  for i:= 0 to Pred(Rows.Count) do
  begin
    if TcxEditorRow(Rows.Items[i]).Properties.EditPropertiesClass = TcxDateEditProperties then
    begin
      SetValue(i, '01/01/1900');
    end;
    if TcxEditorRow(Rows.Items[i]).Properties.EditPropertiesClass = TcxCurrencyEditProperties then
    begin
      SetValue(i, 0);
    end;
    if TcxEditorRow(Rows.Items[i]).Properties.EditPropertiesClass = TcxTextEditProperties then
    begin
      SetValue(i, '');
    end;
  end;
end;

procedure TRVGrid.ResetRows2;
var
  i: Integer;
begin
  for i:= 0 to Pred(Rows.Count) do SetValue(i, '');
end;

procedure TRVGrid.SetValue(AIndex: Integer; AValue: Variant);
begin
  TcxEditorRow(Rows.Items[AIndex]).Properties.Value:= AValue;
end;

{ TRListBox }

procedure TRListBox.ClearExt;
var
  i: Integer;
begin
  for i:= Pred(Items.Count) downto 0 do
  begin
    if Assigned(Items.Objects[i]) then
    begin
      Items.Objects[i].Free;
    end;
  end;
  Items.Clear;
end;

constructor TRListBox.Create(AOwner: TComponent);
begin
  inherited;

  FTempIndex := -1;

  FTimer := TTimer.Create(nil);
  with FTimer do
  begin
    Interval := 10;
    OnTimer := TimerOnTimer;
    Enabled := True;
  end;
end;

destructor TRListBox.Destroy;
begin
  FTimer.Free;
  inherited;
end;

procedure TRListBox.TimerOnTimer(Sender: TObject);
begin
  if ItemIndex <> FTempIndex then
  begin
    if Assigned(FSelectionChanged) then
    begin
      FSelectionChanged(Self, FTempIndex, ItemIndex);
    end;
    FTempIndex := ItemIndex;
  end;
end;

end.
