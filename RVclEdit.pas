unit RVclEdit;

interface

uses
  SysUtils, Classes, cxLabel, Messages, Vcl.Controls, Variants,
  Windows, Vcl.Graphics, Vcl.StdCtrls, cxLookAndFeels, Vcl.Forms,
  cxButtonEdit, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxButtons, RClasses, cxEdit, cxCurrencyEdit, cxCalendar, cxSpinEdit,
  cxMemo, RUtils, RConfig;

type

  TRBaseEdit = class(TcxMaskEdit)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    FOnTriggerValidateExit: TOnTriggerValidate;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;
    procedure KeyPress(var Key: Char); override;
    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

  TREdit = class(TRBaseEdit)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TREditBtn = class(TcxButtonEdit)
  private
    FLabel: TcxLabel;
    FInfoText: TREdit;
    FLabelProps: TRLabelProps;
    FInfoTextProps: TRInfoTextProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
    procedure SetInfoTextProps(const Value: TRInfoTextProps);
    function GetInfoText: string;
    procedure SetInfoText(const Value: string);
    procedure SetCID(const Value: Integer);
    procedure CMVisiblechanged(var Message: TMessage);
    function GetInfoTextProperties: TcxMaskEditProperties;
    procedure SetInfoTextProperties(const Value: TcxMaskEditProperties);
    function GetInfoTextFont: TFont;
    procedure SetInfoTextFont(const Value: TFont);

  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure PropertiesChanged(Sender: TObject); override;
    procedure KeyPress(var Key: Char); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure FontChanged; override;
    procedure VisibleChanged; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoTriggerValidate(xAllowExit: Boolean = True);
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property InfoTextProps: TRInfoTextProps read FInfoTextProps write SetInfoTextProps;
    property InfoText: string read GetInfoText write SetInfoText;
    property InfoTextFont: TFont read GetInfoTextFont write SetInfoTextFont;
    property CID: Integer read FCID write SetCID;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
  end;

  TRNumEdit = class(TcxCurrencyEdit)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

  TRDateEdit = class(TcxDateEdit)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

  TRComboBox = class(TcxComboBox)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AddItem(xKey: Variant; xValue: string);
    function GetItem(xIndex: Integer): TRCbxItem;
    function GetItemStrIndex(xKey: string): Integer;
    function GetKey(xIndex: Integer): Variant;
    function GetActiveKeyAsStr: string;
    function GetActiveKeyAsInt: Integer;
    function GetIndexFromKey(xKey: Variant): Integer;
    procedure ImportList(xoData: TStringList; xAutoFree: Boolean = False);
    procedure ClearItems;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

  TRSpinEdit = class(TcxSpinEdit)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

  TRMemo = class(TcxMemo)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

implementation

{ TRBaseEdit }



procedure TRBaseEdit.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;
  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRBaseEdit.CMFontchange(var Message: TMessage);
begin
  inherited;
  if Properties.MaxLength > 0 then
  begin
    FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
  end;
end;

procedure TRBaseEdit.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
  FLabel.Visible:= Visible;
end;

constructor TRBaseEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;
  BeepOnEnter:= False;

  //inherited Properties.CharCase:= ecUpperCase;

  InitObjects;
end;

destructor TRBaseEdit.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  inherited;
end;

procedure TRBaseEdit.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRBaseEdit.InitObjects;
begin
  if Assigned(FLabel) then Exit;
  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FLabelProps.Font.Name:= LABEL_FONT_NAME;
  FLabelProps.Font.Size:= LABEL_FONT_SIZE;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.AutoSizeField:= False;
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
end;

procedure TRBaseEdit.KeyDown(var Key: Word; Shift: TShiftState);
var
  isMoveTab: Boolean;
begin

  if Key = VK_UP then
  begin
    if Assigned(FOnTriggerKey_ESC) then
    begin
      FOnTriggerKey_ESC(Self, isMoveTab);
      if not isMoveTab then Exit;
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_UP) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
  
  inherited;
end;

procedure TRBaseEdit.KeyPress(var Key: Char);
var
  isMoveTab: Boolean;
begin

  if Key = Chr(VK_UP) then
  begin
    if Assigned(FOnTriggerKey_ESC) then
    begin
      FOnTriggerKey_ESC(Self, isMoveTab);
      if not isMoveTab then Exit;
    end;
  end;

  if FExtProps.NextEnabled then
  begin
    if (Key = Chr(VK_RETURN)) then
    begin
      Key:= #0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

//  if FExtProps.PrevEnabled then
//  begin
//    if (Key = Chr(VK_UP)) then
//    begin
//      Key:= #0;
//      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
//    end;
//  end;

  inherited KeyPress(Key);
end;

procedure TRBaseEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRBaseEdit.PropertiesChanged(Sender: TObject);
begin
  inherited;
  if Properties.MaxLength > 0 then
  begin
    FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
  end;
end;

procedure TRBaseEdit.Resize;
begin
  inherited;
  if Properties.MaxLength > 0 then
  begin
    if FExtProps.AutoSizeField then
    begin
      FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
    end;
  end;
end;

procedure TRBaseEdit.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
end;

procedure TRBaseEdit.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRBaseEdit.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRBaseEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

{ TREdit }

constructor TREdit.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TREdit.Destroy;
begin
  inherited;
end;

{TREditBtn}

procedure TREditBtn.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    xAllowExit:= True;
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TREditBtn.CMFontchange(var Message: TMessage);
begin
  inherited;
  FInfoText.Font:= Font;
  if Properties.MaxLength > 0 then
  begin
    FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
  end;
end;

procedure TREditBtn.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
  InfoTextProps.Visible:= Visible;
end;

constructor TREditBtn.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  //Style.Font.Name:= EDIT_FONT_NAME;
  //Style.Font.Size:= EDIT_FONT_SIZE;

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;
  Properties.CharCase:= ecUpperCase;
  Properties.Buttons.Clear;
  with Properties.Buttons.Add do
  begin
    Width:= 22;
    Kind:= bkEllipsis;
  end;
  Properties.ClickKey:= VK_F1;

  BeepOnEnter:= False;

  InitObjects;
end;

destructor TREditBtn.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  FInfoTextProps.Free;
  inherited;
end;

procedure TREditBtn.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TREditBtn.DoTriggerValidate(xAllowExit: Boolean = True);
var
  isAllow: Boolean;
begin
  isAllow:= xAllowExit;
  if Assigned(FOnTriggerValidateExit) then FOnTriggerValidateExit(Self, isAllow);
end;

procedure TREditBtn.FontChanged;
begin
  inherited;
  //FInfoText.Font:= Font;
end;


function TREditBtn.GetInfoText: string;
begin
  Result:= FInfoText.Text;
end;

function TREditBtn.GetInfoTextFont: TFont;
begin
  Result:= FInfoText.Font;
end;

function TREditBtn.GetInfoTextProperties: TcxMaskEditProperties;
begin
  Result:= FInfoText.Properties;
end;

procedure TREditBtn.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;
  FLabel.FreeNotification(Self);

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 0;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.AutoSizeField:= True;
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;

  if Assigned(FInfoText) then Exit;
  FInfoText:= TREdit.Create(Self);
  FInfoText.LabelProps.Caption:= '';
  FInfoText.LabelProps.Visible:= False;
  FInfoText.ExtProps.AutoSizeField:= True;
  FInfoText.FreeNotification(Self);

  FInfoTextProps:= TRInfoTextProps.Create(Self, FInfoText);
  FInfoTextProps.Position:= ltRight;
  FInfoTextProps.Space:= 2;
  FInfoTextProps.UpdateView;
  FInfoText.Font:= Font;
  FInfoText.Height:= Height;
  FInfoText.LookAndFeel:= LookAndFeel;
end;

procedure TREditBtn.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F1 then
  begin
    if Assigned(Properties.OnButtonClick) then
    begin
      Properties.OnButtonClick(Self, 0);
    end;
  end
  else
  begin
    if FExtProps.PrevEnabled then
    begin
      if (Key = VK_UP) then
      begin
        Key:= 0;
        GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
      end;
    end;

    inherited;
  end;
end;

procedure TREditBtn.KeyPress(var Key: Char);
var
  isMoveTab, isAllowExit: Boolean;
begin
  isMoveTab:= True;

  if Key = Chr(VK_UP) then
  begin
    if Assigned(FOnTriggerKey_ESC) then
    begin
      FOnTriggerKey_ESC(Self, isMoveTab);
      if not isMoveTab then Exit;
    end;
  end;

  if FExtProps.NextEnabled then
  begin
    if (Key = Chr(VK_RETURN)) then
    begin
      Key:= #0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  inherited KeyPress(Key);
end;

procedure TREditBtn.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
  if (AComponent = FInfoText) and (Operation = opRemove) then
    FInfoText:= nil;
end;

procedure TREditBtn.PropertiesChanged(Sender: TObject);
begin
  inherited;
  if Properties.MaxLength > 0 then
  begin
    FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
  end;
end;

procedure TREditBtn.Resize;
begin
  inherited;
  if Properties.MaxLength > 0 then
  begin
    if FExtProps.AutoSizeField then
    begin
      FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
    end;
  end;
end;

procedure TREditBtn.SetCID(const Value: Integer);
begin
  FCID := Value;
end;

procedure TREditBtn.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps := Value;
  if Properties.MaxLength > 0 then
  begin
    if FExtProps.AutoSizeField then
    begin
      FExtProps.AutoSizeField:= FExtProps.AutoSizeField;
    end;
  end;
end;

procedure TREditBtn.SetInfoText(const Value: string);
begin
  FInfoText.Text:= Value;
end;

procedure TREditBtn.SetInfoTextFont(const Value: TFont);
begin

end;

procedure TREditBtn.SetInfoTextProperties(const Value: TcxMaskEditProperties);
begin

end;

procedure TREditBtn.SetInfoTextProps(const Value: TRInfoTextProps);
begin
  if FInfoTextProps <> Value then
  begin
    FInfoTextProps:= Value;
    FInfoTextProps.UpdateView;
    SetInfoText(FInfoText.Text);
  end;
end;

procedure TREditBtn.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  FLabelProps.UpdateView;
end;

procedure TREditBtn.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;

  if FInfoText = nil then Exit;
  FInfoText.Parent:= AParent;
end;

procedure TREditBtn.VisibleChanged;
begin
  inherited;
  FLabelProps.Visible:= Visible;
  FInfoText.Visible:= Visible;
end;

procedure TREditBtn.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
  FInfoTextProps.UpdateView;
  FLabel.Invalidate;
  FInfoText.Style.Font:= Style.Font;
  FInfoText.LookAndFeel:= LookAndFeel;
end;

{TRNumEdit}

procedure TRNumEdit.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;
  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRNumEdit.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRNumEdit.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
end;

constructor TRNumEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= EDIT_FONT_NAME;
  Style.Font.Size:= EDIT_FONT_SIZE;

  Properties.DisplayFormat:= '#,###0.00';
  Properties.EditFormat:= '#,###0.00';
  Properties.ValidationOptions:= [];
  Properties.UseLeftAlignmentOnEditing:= False;
  Properties.Alignment.Horz:= taRightJustify;
  Properties.UseDisplayFormatWhenEditing:= True;
  Properties.UseThousandSeparator:= True;
  Properties.Alignment.Horz:= taRightJustify;
  Properties.Nullable:= False;
  Properties.ValidateOnEnter:= False;

  Value:= 0;

  InitObjects;
end;

destructor TRNumEdit.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  inherited;
end;

procedure TRNumEdit.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PasteEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRNumEdit.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.AutoSizeField:= False;
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
end;

procedure TRNumEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_UP) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRNumEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRNumEdit.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRNumEdit.Resize;
begin
  inherited;
end;

procedure TRNumEdit.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps := Value;

  if FExtProps.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRNumEdit.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRNumEdit.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRNumEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

{TRDateEdit}

procedure TRDateEdit.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;

  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRDateEdit.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRDateEdit.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
end;

constructor TRDateEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;

  Properties.ValidateOnEnter:= False;
  Properties.Animation:= True;
  Properties.AssignedValues.EditFormat:= True;
  Properties.DateButtons:= [btnToday];
  Properties.DisplayFormat:= 'dd/MM/yyyy';
  Properties.EditFormat:= 'dd/MM/yyyy';
  Properties.ImmediatePost:= True;
  Properties.View:= cavDefault;
  Date:= Now;

  BeepOnEnter:= False;

  InitObjects;
end;

destructor TRDateEdit.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  inherited;
end;

procedure TRDateEdit.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRDateEdit.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
  FExtProps.AutoSizeField:= False;
end;

procedure TRDateEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;

  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_UP) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRDateEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRDateEdit.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRDateEdit.Resize;
begin
  inherited;
end;

procedure TRDateEdit.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
  if Value.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRDateEdit.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRDateEdit.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRDateEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

{TRComboBox}

procedure TRComboBox.ClearItems;
var
  i: Integer;
begin
  for i:= 0 to Pred(Self.Properties.Items.Count) do
  begin

    if Self.Properties.Items.Objects[i] is TRCbxItem then
    begin
      TRCbxItem(Self.Properties.Items.Objects[i]).Free;
    end;
  end;
  Self.Properties.Items.Clear;
end;

procedure TRComboBox.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRComboBox.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRComboBox.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= False;
end;

constructor TRComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;
  Properties.ValidateOnEnter:= False;

  BeepOnEnter:= False;

  Properties.ImmediatePost:= True;
  Properties.ImmediateUpdateText:= True;

  inherited Properties.DropDownListStyle:= lsEditFixedList;

  InitObjects;
end;

destructor TRComboBox.Destroy;
var
  i: Integer;
begin
  FLabelProps.Free;
  FExtProps.Free;

  for i:= 0 to Pred(Properties.Items.Count) do
  begin
    if Assigned(Properties.Items.Objects[i]) then Properties.Items.Objects[i].Free;
  end;

  inherited Destroy;
end;

procedure TRComboBox.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRComboBox.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
  FExtProps.AutoSizeField:= False;
end;

procedure TRComboBox.KeyDown(var Key: Word; Shift: TShiftState);
var
  isMoveTab: Boolean;
begin
  inherited KeyDown(Key, Shift);

  if Key = VK_UP then
  begin
    if Assigned(FOnTriggerKey_ESC) then
    begin
      FOnTriggerKey_ESC(Self, isMoveTab);
      if not isMoveTab then Exit;
    end;
  end;

  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_UP) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRComboBox.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRComboBox.Resize;
begin
  inherited;
end;

procedure TRComboBox.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
  if Value.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRComboBox.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRComboBox.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRComboBox.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

procedure TRComboBox.AddItem(xKey: Variant; xValue: string);
begin
  with Properties.Items do
  begin
    AddObject(xValue, TRCbxItem.Create(xKey, xValue));
  end;
end;

function TRComboBox.GetActiveKeyAsInt: Integer;
var
  xTmp: Variant;
begin
  if ItemIndex = -1 then
  begin
    Result:= 0;
    Exit;
  end;

  xTmp:= GetKey(ItemIndex);
  Result:= VariantToInt(xTmp);
end;

function TRComboBox.GetActiveKeyAsStr: string;
var
  xTmp: Variant;
begin
  if ItemIndex = -1 then
  begin
    Result:= '';
    Exit;
  end;

  xTmp:= GetKey(ItemIndex);
  Result:= VariantToStr(xTmp);
end;

function TRComboBox.GetIndexFromKey(xKey: Variant): Integer;
begin
  var xResult := -1;
  var i: Integer;
  for i := 0 to Properties.Items.Count - 1 do
  begin
    var oData: TRCbxItem;
    oData := GetItem(i);
    if (oData <> nil) then
    begin
      if (oData.Key = xKey) then
      begin
        xResult := i;
        oData.Free;
        Break;
      end;
    end;
    oData.Free;
  end;

  Result := xResult;
end;

function TRComboBox.GetItem(xIndex: Integer): TRCbxItem;
begin
  Result:= TRCbxItem.Create('', '');
  if Assigned(Properties.Items.Objects[xIndex]) then
  begin
    Result.Key:= TRCbxItem(Properties.Items.Objects[xIndex]).Key;
    Result.DisplayText:= TRCbxItem(Properties.Items.Objects[xIndex]).DisplayText;
  end;
end;

function TRComboBox.GetItemstrIndex(xKey: string): Integer;
var
  xRetval: Integer;
  i: Integer;
begin
  xRetval:= -1;

  if Properties.Items.Count > 0 then
  begin
    for i:= 0 to Pred(Properties.Items.Count) do
    begin
      if SameText(VariantToStr(GetKey(i)), xKey) then
      begin
        xRetval:= i;
        Break;
      end;
    end;
  end;

  Result:= xRetval;
end;

function TRComboBox.GetKey(xIndex: Integer): Variant;
var
  xTmp: Variant;
  oData: TRCbxItem;
begin
  oData:= GetItem(xIndex);
  try
    xTmp:= oData.Key;
  finally
    oData.Free;
  end;

  Result:= xTmp;
end;

procedure TRComboBox.ImportList(xoData: TStringList; xAutoFree: Boolean = False);
var
  i: Integer;
  xName: string;
begin
  if xoData = nil then Exit;

  for i:= 0 to Pred(Properties.Items.Count) do
  begin
    if Assigned(Properties.Items.Objects[i]) then Properties.Items.Objects[i].Free;
  end;
  Properties.Items.Clear;

  for i:= 0 to Pred(xoData.Count) do
  begin
    xName:= xoData.Names[i];
    //if Trim(xName) <> '' then
    //begin
      AddItem(xName, xoData.Values[xName]);
    //end;
  end;

  if xAutoFree then xoData.Free;
end;

{TRSpinEdit}

procedure TRSpinEdit.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;

  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRSpinEdit.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRSpinEdit.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
end;

constructor TRSpinEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;

  Properties.ValidateOnEnter:= False;
  Properties.ImmediatePost:= True;
  Properties.UseLeftAlignmentOnEditing:= False;
  Properties.Alignment.Horz:= taRightJustify;
  Properties.UseDisplayFormatWhenEditing:= True;

  BeepOnEnter:= False;

  InitObjects;
end;

destructor TRSpinEdit.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  inherited;
end;

procedure TRSpinEdit.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRSpinEdit.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
  FExtProps.AutoSizeField:= False;
end;

procedure TRSpinEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;

  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_UP) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRSpinEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRSpinEdit.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRSpinEdit.Resize;
begin
  inherited;
end;

procedure TRSpinEdit.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
  if Value.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRSpinEdit.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRSpinEdit.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRSpinEdit.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

{TRMemo}

procedure TRMemo.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;

  xAllowExit:= True;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRMemo.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRMemo.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= Visible;
end;

constructor TRMemo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Verdana';
  Style.Font.Size:= 8;

  Properties.ValidateOnEnter:= False;
  Properties.ImmediatePost:= True;
  Properties.WantReturns:= False;
  Properties.WantTabs:= False;

  BeepOnEnter:= False;

  InitObjects;
end;

destructor TRMemo.Destroy;
begin
  FLabelProps.Free;
  FExtProps.Free;
  inherited;
end;

procedure TRMemo.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRMemo.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
  FExtProps.AutoSizeField:= False;
end;

procedure TRMemo.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;

  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
//    if (Key = VK_UP) then
//    begin
//      Key:= 0;
//      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
//    end;
    if Key = VK_UP then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRMemo.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRMemo.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRMemo.Resize;
begin
  inherited;
end;

procedure TRMemo.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
  if Value.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRMemo.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRMemo.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRMemo.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

end.
