unit cxGrid1;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, cxControls, cxGrid;

type
  TcxGrid1 = class(TcxGrid)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TcxGrid1]);
end;

end.
