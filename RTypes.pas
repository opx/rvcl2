unit RTypes;

interface

uses
  Classes, SysUtils;

type
  TREditorTyp = (etString, etDate, etNumber, etNumberFmt0, etNumberFmt2);
  TREditorOpt = (eoNoFocus);
  TREditorOpts = set of TREditorOpt;

implementation

end.
