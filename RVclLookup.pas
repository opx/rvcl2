unit RVclLookup;

interface

uses
  SysUtils, Classes, Variants, RUtils,
  Windows, Vcl.StdCtrls, Vcl.Controls, Vcl.Forms, Messages, RClasses,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxDBExtLookupComboBox,
  cxLabel;

type
  TRLookupComboBox = class(TcxExtLookUpComboBox)
  private
    FLabel: TcxLabel;
    FLabelProps: TRLabelProps;
    FExtProps: TRExtEditProps;
    FCID: Integer;
    FOnTriggerValidateExit: TOnTriggerValidate;
    FOnTriggerKey_ESC: TOnTriggerKeyTab;
    procedure InitObjects;
    procedure SetLabelProps(const Value: TRLabelProps);
    procedure SetExtProps(const Value: TRExtEditProps);
  protected
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Resize; override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMVisiblechanged(var Message: TMessage);
      message CM_VISIBLECHANGED;
    procedure PropertiesChanged(Sender: TObject); override;

    procedure CMFontchange(var Message: TMessage); message CM_FONTCHANGE;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AddItem(xKey: Variant; xValue: string);
    function GetItem(xIndex: Integer): TRCbxItem;
    function GetItemStrIndex(xKey: string): Integer;
    function GetKey(xIndex: Integer): Variant;
    function GetActiveKeyAsStr: string;
    function GetActiveKeyAsInt: Integer;
    procedure ImportList(xoData: TStringList);
  published
    property LabelProps: TRLabelProps read FLabelProps write SetLabelProps;
    property ExtProps: TRExtEditProps read FExtProps write SetExtProps;
    property CID: Integer read FCID write FCID;
    property OnTriggerKey_ESC: TOnTriggerKeyTab read FOnTriggerKey_ESC write FOnTriggerKey_ESC;
    property OnTriggerValidate: TOnTriggerValidate read FOnTriggerValidateExit write FOnTriggerValidateExit;
  end;

implementation

{TRLookupComboBox}

procedure TRLookupComboBox.CMExit(var Message: TCMExit);
var
  xAllowExit: Boolean;
begin
  inherited;
  if Assigned(FOnTriggerValidateExit) then
  begin
    FOnTriggerValidateExit(Self, xAllowExit);
    if xAllowExit then
    begin
      inherited;
    end
    else
    begin
      Self.SetFocus;
    end;
  end;
end;

procedure TRLookupComboBox.CMFontchange(var Message: TMessage);
begin
  inherited;
end;

procedure TRLookupComboBox.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  FLabelProps.Visible:= False;
end;

constructor TRLookupComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style.Font.Name:= 'Consolas';
  Style.Font.Size:= 10;
  Properties.ValidateOnEnter:= False;

  BeepOnEnter:= False;

  Properties.ImmediatePost:= True;
  Properties.ImmediateUpdateText:= True;

  InitObjects;
end;

destructor TRLookupComboBox.Destroy;
var
  i: Integer;
begin
  FLabelProps.Free;
  FExtProps.Free;

  for i:= 0 to Pred(Properties.Items.Count) do
  begin
    if Assigned(Properties.Items.Objects[i]) then Properties.Items.Objects[i].Free;
  end;

  inherited Destroy;
end;

procedure TRLookupComboBox.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FExtProps.PopupEnabled then
  begin
    inherited;
  end
  else
  begin
    Handled:= True;
  end;
end;

procedure TRLookupComboBox.InitObjects;
begin
  if Assigned(FLabel) then Exit;

  FLabel:= TcxLabel.Create(Self);
  FLabel.Transparent:= True;

  FLabelProps:= TRLabelProps.Create(FLabel, Self);
  FLabelProps.Position:= lpLeft;
  FLabelProps.PosAlign:= laMid;
  FLabelProps.Space:= 2;
  FLabelProps.UpdateView;

  FExtProps:= TRExtEditProps.Create(Self);
  FExtProps.NextEnabled:= True;
  FExtProps.PrevEnabled:= True;
  FExtProps.AutoSizeField:= False;
end;

procedure TRLookupComboBox.KeyDown(var Key: Word; Shift: TShiftState);
var
  isMoveTab: Boolean;
begin
  inherited KeyDown(Key, Shift);

  if Key = VK_ESCAPE then
  begin
    if Assigned(FOnTriggerKey_ESC) then
    begin
      FOnTriggerKey_ESC(Self, isMoveTab);
      if not isMoveTab then Exit;
    end;
  end;

  if FExtProps.NextEnabled then
  begin
    if (Key = VK_RETURN) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;

  if FExtProps.PrevEnabled then
  begin
    if (Key = VK_ESCAPE) then
    begin
      Key:= 0;
      GetParentForm(Self).Perform(WM_NEXTDLGCTL, 1, 0);
    end;
  end;
end;

procedure TRLookupComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (AComponent = FLabel) and (Operation = opRemove) then
    FLabel := nil;
end;

procedure TRLookupComboBox.PropertiesChanged(Sender: TObject);
begin
  inherited;
end;

procedure TRLookupComboBox.Resize;
begin
  inherited;
end;

procedure TRLookupComboBox.SetExtProps(const Value: TRExtEditProps);
begin
  FExtProps:= Value;
  if Value.AutoSizeField then
  begin
    FExtProps.AutoSizeField:= False;
  end;
end;

procedure TRLookupComboBox.SetLabelProps(const Value: TRLabelProps);
begin
  FLabelProps := Value;
  if FLabelProps <> Value then
  begin
    FLabelProps.UpdateView;
  end;
end;

procedure TRLookupComboBox.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FLabel = nil then Exit;
  FLabel.Parent:= AParent;
end;

procedure TRLookupComboBox.WMPaint(var Message: TWMPaint);
begin
  inherited;
  FLabelProps.UpdateView;
end;

procedure TRLookupComboBox.AddItem(xKey: Variant; xValue: string);
begin
  with Properties.Items do
  begin
    AddObject(xValue, TRCbxItem.Create(xKey, xValue));
  end;
end;

function TRLookupComboBox.GetActiveKeyAsInt: Integer;
var
  xTmp: Variant;
begin
  if ItemIndex = -1 then
  begin
    Result:= 0;
    Exit;
  end;

  xTmp:= GetKey(ItemIndex);
  Result:= VariantToInt(xTmp);
end;

function TRLookupComboBox.GetActiveKeyAsStr: string;
var
  xTmp: Variant;
begin
  if ItemIndex = -1 then
  begin
    Result:= '';
    Exit;
  end;

  xTmp:= GetKey(ItemIndex);
  Result:= VariantToStr(xTmp);
end;

function TRLookupComboBox.GetItem(xIndex: Integer): TRCbxItem;
begin
  Result:= TRCbxItem.Create('', '');
  if Assigned(Properties.Items.Objects[xIndex]) then
  begin
    Result.Key:= TRCbxItem(Properties.Items.Objects[xIndex]).Key;
    Result.DisplayText:= TRCbxItem(Properties.Items.Objects[xIndex]).DisplayText;
  end;
end;

function TRLookupComboBox.GetItemstrIndex(xKey: string): Integer;
var
  xRetval: Integer;
  i: Integer;
begin
  xRetval:= -1;

  if Properties.Items.Count > 0 then
  begin
    for i:= 0 to Pred(Properties.Items.Count) do
    begin
      if SameText(VariantToStr(GetKey(i)), xKey) then
      begin
        xRetval:= i;
        Break;
      end;
    end;
  end;

  Result:= xRetval;
end;

function TRLookupComboBox.GetKey(xIndex: Integer): Variant;
var
  xTmp: Variant;
  oData: TRCbxItem;
begin
  oData:= GetItem(xIndex);
  try
    xTmp:= oData.Key;
  finally
    oData.Free;
  end;

  Result:= xTmp;
end;

procedure TRLookupComboBox.ImportList(xoData: TStringList);
var
  i: Integer;
  xName: string;
begin
  if xoData = nil then Exit;

  for i:= 0 to Pred(Properties.Items.Count) do
  begin
    if Assigned(Properties.Items.Objects[i]) then Properties.Items.Objects[i].Free;
  end;
  Properties.Items.Clear;

  for i:= 0 to Pred(xoData.Count) do
  begin
    xName:= xoData.Names[i];
    if Trim(xName) <> '' then
    begin
      AddItem(xName, xoData.Values[xName]);
    end;
  end;
end;

end.
