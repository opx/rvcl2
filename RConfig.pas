unit RConfig;

interface

uses
  IniFiles, SysUtils, Classes;

var
  LABEL_FONT_NAME: string;
  LABEL_FONT_SIZE: Integer;
  EDIT_FONT_NAME: string;
  EDIT_FONT_SIZE: Integer;

implementation

const
  SECTION = 'APPCONFIG';

procedure ReadConfig;
var
  xFile: string;
  oIni: TIniFile;
begin
  xFile:= ExtractFilePath(ParamStr(0)) + 'ui.config';
  oIni:= TIniFile.Create(xFile);
  try
    LABEL_FONT_NAME:= oIni.ReadString(SECTION, 'LABEL_FONT', 'Verdana');
    LABEL_FONT_SIZE:= oIni.ReadInteger(SECTION, 'LABEL_SIZE', 8);
    EDIT_FONT_NAME:= oIni.ReadString(SECTION, 'EDIT_FONT', 'Verdana');
    EDIT_FONT_SIZE:= oIni.ReadInteger(SECTION, 'EDIT_SIZE', 8);
  finally
    oIni.Free;
  end;
end;

initialization
  ReadConfig;

end.
